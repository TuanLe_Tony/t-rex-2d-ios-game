//
//  GameScene.swift
//  T-rex
//
//  Created by Tony on 2019-02-16.

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    //JUMPING SETTING Y AND X JUMPING (^.-)
    let jumpUp = SKAction.moveBy(x: 0, y: 420, duration: 0.4)
    let fallBack = SKAction.moveBy(x: 0, y: -420, duration: 0.4)
    var jumpAction = SKAction()


    //ANIMATION
    var MainTrex = SKSpriteNode()
    var TexttureAtlas = SKTextureAtlas()
    var TextureArray = [SKTexture]()
    
    var Fireball = SKSpriteNode()
    var TexttureAtlas1 = SKTextureAtlas()
    var TextureArray1 = [SKTexture]()
    
    var FireShot = SKSpriteNode()
    var TexttureAtlas2 = SKTextureAtlas()
    var TextureArray2 = [SKTexture]()
    //----END---\\
    var win = 20;
    var openCactuses = false
    var isRunning = true
    // MARK: Sprites
    // ----------------------
    let Trex = SKSpriteNode(imageNamed: "trex200px")
    let TrexBackground = SKSpriteNode(imageNamed: "backgroundtrex")
    let TrexBackgroundFollow = SKSpriteNode(imageNamed: "backgroundtrex1")
    let Cactus = SKSpriteNode(imageNamed: "cactus180px")
    let rock = SKSpriteNode(imageNamed: "rock")
    let cloud = SKSpriteNode(imageNamed: "cloud")
    let cloud1 = SKSpriteNode(imageNamed: "cloud1")
    let meat = SKSpriteNode(imageNamed: "meat")
    let meattop = SKSpriteNode(imageNamed: "meattop")
    let meatlabel = SKSpriteNode(imageNamed: "meatlabel")
    let fireshotimage = SKSpriteNode(imageNamed: "fireshot_1")
    // MARK: Game statics variables
    // ----------------------
    var FireBullet = SKLabelNode(text: "")
    var PointCollectedLabel = SKLabelNode(text: "")
    
    var points = 0
    var bullet = 0
    var meatpoint = CGFloat(1)
    let actualPlayfieldRect:CGRect
    let SHOW_BOUNDARIES = true
    var Speed = CGFloat(25)
    var FireballSpeed = CGFloat(25)
    
    // Setting sound effect
    let jumbSound = SKAction.playSoundFileNamed("Audio/pop.flac", waitForCompletion: false)
    let eatSound = SKAction.playSoundFileNamed("Audio/eat.mp3", waitForCompletion: false)
    let shootSound = SKAction.playSoundFileNamed("Audio/shoot.flac", waitForCompletion: false)
    
    override init(size: CGSize) {
        // if this was a 4:3 screen, change it to 4.0/3.0
        let maxAspectRatio:CGFloat = 16.0/9.0
        let playfieldHeight = size.width / maxAspectRatio
        let margins = (size.height - playfieldHeight) / 2.0
        self.actualPlayfieldRect = CGRect(
            x: 0,
            y: margins,
            width: size.width,
            height: playfieldHeight)
        super.init(size:size)
    }
    func drawBoundaries() {
        // 1. Create a rectangle
        let rect = SKShapeNode()
        let path = CGMutablePath()
        path.addRect(self.actualPlayfieldRect)
        rect.path = path
        
        // 2. Congfigure the rectangle
        rect.strokeColor = SKColor.magenta
        rect.lineWidth = 10.0
        
        // 3. draw the rectangle on the screen
        addChild(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var showCactuses = false
    
    override func didMove(to view: SKView) {
        
    
        //JUMPING ACTION
        jumpAction = SKAction.sequence([jumpUp, fallBack])
        
        // Set the background color of the app
        self.backgroundColor = SKColor.white;
        //Fill empty space
        self.TrexBackgroundFollow.position = CGPoint(x: self.size.width/2*3, y: self.size.height/2)
        addChild(TrexBackgroundFollow)

        // Background T-rex double fugly
        self.TrexBackground.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        addChild(TrexBackground)
        
        
        //ANIMATION FUGLY FIRE SHOT :)
        TexttureAtlas2 = SKTextureAtlas(named: "shotball")
        for i in 1...TexttureAtlas2.textureNames.count{
            let Name2 = "fireshot_\(i).png"
            
            TextureArray2.append(SKTexture(imageNamed: Name2))
        }
        FireShot = SKSpriteNode(imageNamed: TexttureAtlas2.textureNames[0] )
        FireShot.size = CGSize(width: 80, height: 40)
        FireShot.position = CGPoint(x: 220, y:510)
        
        //hitbox
        self.FireShot.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.FireShot.frame.width, height:self.FireShot.frame.height))
        self.FireShot.physicsBody?.affectedByGravity = false
        self.FireShot.physicsBody?.isDynamic = false
        self.addChild(FireShot)
        //end hitbox
        
        //T-rex is running foever :D ( poor T - work hard :(.
        FireShot.run(SKAction.repeatForever(
            SKAction.animate(with: TextureArray2,
                             timePerFrame: 0.02,
                             resize: false,
                             restore: true)),
                     withKey:"Fireshot")
        //-----------------END ANIMATION----------\\
    
        
        //ANIMATION FUGLY T_REX :)
        TexttureAtlas = SKTextureAtlas(named: "Images")
        for i in 1...TexttureAtlas.textureNames.count{
            let Name1 = "trex_\(i).png"
        
            TextureArray.append(SKTexture(imageNamed: Name1))
        }
        MainTrex = SKSpriteNode(imageNamed: TexttureAtlas.textureNames[0] )
        
        MainTrex.size = CGSize(width: 150, height: 130)
        MainTrex.position = CGPoint(x: 200, y: 500)
        
        //hitbox
        self.MainTrex.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.MainTrex.frame.width, height:self.MainTrex.frame.height))
        self.MainTrex.physicsBody?.affectedByGravity = false
        self.MainTrex.physicsBody?.isDynamic = false
        //end hitbox
        
        //T-rex is running foever :D ( poor T - work hard :(.
        self.addChild(MainTrex)
        MainTrex.run(SKAction.repeatForever(
            SKAction.animate(with: TextureArray,
                             timePerFrame: 0.05,
                             resize: false,
                             restore: true)),
                    withKey:"MainTrex")
        //-----------------END ANIMATION----------\\
        
        
        //ANIMATION FUGLY FIRE BALL :)
        TexttureAtlas1 = SKTextureAtlas(named: "fireball")
        for i in 1...TexttureAtlas1.textureNames.count{
            let Name = "ball_\(i).png"
            
            TextureArray1.append(SKTexture(imageNamed: Name))
        }
        Fireball = SKSpriteNode(imageNamed: TexttureAtlas1.textureNames[0] )
            
        Fireball.size = CGSize(width: 90, height: 90)
        Fireball.position = CGPoint(x: self.size.width - 50, y:460)
        
        //hitbox
        self.Fireball.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.Fireball.frame.width, height:self.Fireball.frame.height))
        self.Fireball.physicsBody?.affectedByGravity = false
        self.Fireball.physicsBody?.isDynamic = false
        self.addChild(Fireball)
        //end hitbox
        
        //T-rex is running foever :D ( poor T - work hard :(.
        Fireball.run(SKAction.repeatForever(
            SKAction.animate(with: TextureArray1,
                             timePerFrame: 0.02,
                             resize: false,
                             restore: true)),
                     withKey:"Fireball")
        //-----------------END ANIMATION----------\\
        
        // Cactus
        self.Cactus.size = CGSize(width: 50, height: 110)
        self.Cactus.position = CGPoint(x: self.size.width-300, y:480)
        // - add a hitbox to the cactus
        self.Cactus.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.Cactus.frame.width, height:self.Cactus.frame.height))
        self.Cactus.physicsBody?.affectedByGravity = false
        self.Cactus.physicsBody?.isDynamic = false
        addChild(Cactus)
        
        
        // ROCKs
    
        self.rock.size = CGSize(width: 50, height: 130)
        self.rock.position = CGPoint(x: self.size.width/2 * 3, y:480)
        // - add a hitbox to the zombie
        self.rock.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.rock.frame.width, height:self.rock.frame.height))
        self.rock.physicsBody?.affectedByGravity = false
        self.rock.physicsBody?.isDynamic = false
        addChild(rock)
        
        
        // cloud
        self.cloud.position = CGPoint(x: self.size.width - 200, y:850)
        addChild(cloud)
        
        // cloud1
        self.cloud1.position = CGPoint(x: self.size.width - 400, y:880)
        addChild(cloud1)
        
        // meat below
        
        self.meat.size = CGSize(width: 80, height: 60)
        self.meat.position = CGPoint(x: self.size.width - 800, y:480)
        self.meat.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.meat.frame.width, height:self.meat.frame.height))
        self.meat.physicsBody?.affectedByGravity = false
        self.meat.physicsBody?.isDynamic = false
        addChild(meat)
        
        //meat above
        self.meattop.size = CGSize(width: 80, height: 60)
        self.meattop.position = CGPoint(x: self.size.width - 600, y:850)
        self.meattop.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.meattop.frame.width, height:self.meattop.frame.height))
        self.meattop.physicsBody?.affectedByGravity = false
        self.meattop.physicsBody?.isDynamic = false
        addChild(meattop)
        
        
        
        // meats collected
        self.PointCollectedLabel.text = "\(self.points)"
        self.PointCollectedLabel.fontName = "AvenirNext-Bold"
        self.PointCollectedLabel.fontSize = 100
        self.PointCollectedLabel.fontColor = UIColor.black
        self.PointCollectedLabel.position = CGPoint(x: self.actualPlayfieldRect.minX+1600, y: self.actualPlayfieldRect.maxY-200)
        addChild(PointCollectedLabel)
        
        // meats collected
        self.FireBullet.text = "\(self.bullet)"
        self.FireBullet.fontName = "AvenirNext-Bold"
        self.FireBullet.fontSize = 100
        self.FireBullet.fontColor = UIColor.black
        self.FireBullet.position = CGPoint(x: self.actualPlayfieldRect.minX+200, y: self.actualPlayfieldRect.maxY-200)
        addChild(FireBullet)
        
        //meat label
        self.meatlabel.size = CGSize(width: 120, height: 100)
        self.meatlabel.position = CGPoint(x:self.size.width - 250, y:1176)
        addChild(meatlabel)
        
        //meat label
        self.fireshotimage.size = CGSize(width: 120, height: 40)
        self.fireshotimage.position = CGPoint(x:100, y:1176)
        addChild(fireshotimage)
        

    }
    var isShooting = false
    override func update(_ currentTime: TimeInterval) {
        
        // SHOOTTING
        if (isShooting == true){
            self.FireShot.position.x = self.FireShot.position.x + 50
        }
        
        //Update fugly sprites position :D
        self.TrexBackground.position.x = self.TrexBackground.position.x - CGFloat(self.Speed);
        self.TrexBackgroundFollow.position.x = self.TrexBackgroundFollow.position.x - CGFloat(self.Speed);
        self.Cactus.position.x = self.Cactus.position.x - self.Speed
        self.rock.position.x = self.rock.position.x - self.Speed
        self.cloud.position.x = self.cloud.position.x - self.Speed
        self.cloud1.position.x = self.cloud1.position.x - self.Speed
        self.meat.position.x = self.meat.position.x - self.Speed
        self.meattop.position.x = self.meattop.position.x - self.Speed
        self.Fireball.position.x = self.Fireball.position.x - (self.Speed + FireballSpeed)
        // --------------- END ------------------ \\
        
        // IF STATEMENT WORK-PLACE :)
        if (self.cloud.position.x <= 0){
            let randomX = arc4random_uniform(UInt32(self.size.width + 800))
            let randomY = arc4random_uniform(UInt32(self.actualPlayfieldRect.maxY - self.actualPlayfieldRect.minY)) + UInt32(self.actualPlayfieldRect.minY)
            self.cloud.position.x = CGFloat(randomX)
            self.cloud.position.x = CGFloat(randomY)
            
            
            // DEBUG:  output new position of cat
            print("Random (x,y): \(randomX), \(randomY) ")
        }
        if (self.cloud1.position.x <= 0) {
            self.cloud1.position = CGPoint(x: self.size.width + 400, y:888)
        }
        //When I reset in this the singel cactus(image) will repeat exactly position that been set up.
        // Make Other obstacles randomly with cactus.
        if (self.Cactus.position.x <= 0){
            //reposition.
            resetOjectrunning();
        }
        // ---- END RESEt ---  \\

        if(self.cloud1.position.x <= 0 ){
            self.cloud1.position = CGPoint(x: self.size.width + 500, y:480)
        }
        if ((self.MainTrex.frame.intersects(self.meat.frame))) {
            print("Meat below got eaten or left!")
            //reset meat
            self.meat.position = CGPoint(x: self.size.width + 1300, y:480)
            //set point to label
            self.points = self.points + Int(meatpoint);
            self.PointCollectedLabel.text = "\(self.points)"
            
            run(eatSound) //play sound for eat
            
            if (self.points == 5){
                self.bullet = self.bullet + 1
                self.FireBullet.text = "\(self.bullet)"
            }
        }
        if ((self.MainTrex.frame.intersects(self.meattop.frame)) ) {
            print("Meat above got eaten or left!")
            
            self.meattop.position = CGPoint(x: self.size.width + 1000, y:860)
            
            self.points = self.points + Int(meatpoint);
            self.PointCollectedLabel.text = "\(self.points)"
            
            run(eatSound) //play sound for eat
            
            if (self.points == 5 || self.points == 10 || self.points == 10){
                self.bullet = self.bullet + 1
                self.FireBullet.text = "\(self.bullet)"
            }
        }
        //check meat top touch x = 0
        if ((self.meattop.position.x <= 0)){
            self.meattop.position = CGPoint(x: self.size.width + 1000, y:860)
        }
        //check meat below touch x = 0
        if ((self.meat.position.x <= 0)){
            self.meat.position = CGPoint(x: self.size.width + 1300, y:860)
        }
//        if (self.Fireball.position.x <= 0) {
//            self.Fireball.position = CGPoint(x: self.size.width + 1000, y:480)
//
//        }
        
        // MARK: If fugly T-rex kisses cactus
        // -------------------WIN AND LOSE------------------
        if (self.MainTrex.frame.intersects(self.Cactus.frame) || self.MainTrex.frame.intersects(self.rock.frame) ||
            self.MainTrex.frame.intersects(self.Fireball.frame)) {


            print("COLLISION!")

            print("YOU LOSE!")
            

            let scene = GameScene(size: self.size)
            scene.scaleMode = .aspectFill
            let animation = SKTransition.fade(withDuration: 2.0)
            self.view?.presentScene(scene, transition: animation)
            self.removeAllChildren()
            self.removeAllActions()
            self.scene?.removeFromParent()
        }
        
        if (points == win){
            print("Won! Won! Won!")
            // 1. Initialize the new scene
            let gameOverScene = GameOverScene(size:self.size, win:true)
            gameOverScene.scaleMode = self.scaleMode
            
            // 2. Configure the "animation" between screens
            let transitionEffect = SKTransition.flipHorizontal(withDuration: 3)
            
            // 3. Show the scene
            self.view?.presentScene(gameOverScene, transition: transitionEffect)
        }
        
        if (self.Cactus.frame.intersects(self.FireShot.frame)) {
            print("FIREEEEEEE DESTrOy!")
            
            self.Cactus.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        
        if (self.FireShot.position.x >= self.size.width/2){
            
                self.FireShot.position = CGPoint(x: 220, y:510)
                self.bullet = self.bullet - 1
                self.FireBullet.text = "\(self.bullet)"
                isShooting = false
            

        }
        if (self.FireShot.frame.intersects(self.Cactus.frame)) {
            print("FIREEEEEEE DESTrOy!")
            self.FireShot.position = CGPoint(x: 220, y:510)
        }
        
        updateAppear()
        
    }
    func updateAppear() {
        /// UPDATE FIRE BALL
        if (self.points == 5){
            self.Fireball.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        if (self.points == 10) {
            self.Fireball.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        if (self.points == 15) {
            self.Fireball.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        // UPDATE ROCK
        if (self.points == 2){
            self.rock.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        if (self.points == 4){
            self.rock.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        if (self.points == 13){
            self.rock.position = CGPoint(x: self.size.width + 1000, y:480)
        }
        
        
        
    }

    //Reset fugly things kk
    func resetOjectrunning() {
        // reset the Cactu
         self.Cactus.position = CGPoint(x: self.size.width-100, y:480)
         self.TrexBackground.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
         self.TrexBackgroundFollow.position = CGPoint(x: self.size.width/2*3, y: self.size.height/2)
    }
    //touching work place :D
    //jumb once time
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let location = touch.location(in: self)
            
            if(location.x > self.frame.size.width/2){
                
                if (self.bullet >= 1){
                    isShooting = true
                
                if (self.FireShot.frame.intersects(self.Cactus.frame)) {
                    print("FIREEEEEEE DESTrOy!")
                    self.FireShot.position = CGPoint(x: 220, y:510)
                }
                
                }//END IF STATEMENT
                
                print("Left")
                
                
            }
                
            else if(location.x < self.frame.size.width/2){
                
                if MainTrex.action(forKey: "jump") == nil { // check that there's no jump action running
                    MainTrex.run(jumpAction, withKey:"jump")
                    run(jumbSound)
                    print("Right")
                }
                if FireShot.action(forKey: "jump") == nil { // check that there's no jump action running
                    FireShot.run(jumpAction, withKey:"jump")
                    print("Right")
                }
            }
        }
    }

}
